# Repo organisation

- `main.tex` is the master .tex file.
- `sections/` is the directory containing one latex file per section
- `images/` contains all figures
- `supplementary-materials` contains the 2 supplementary materials that come along the article
- `libs/` contains tools used to generate figures.

# Compiling

This document can be compiled using lualatex and biber

    lualatex -pdf -shell-escape main.tex
    biber main
    lualatex -pdf -shell-escape main.tex
    lualatex -pdf -shell-escape main.tex
    

# Contributing

Any contribution should have an entry in the changelog at the end of the document. See section 

```latex
\begin{changelog}
...
\end{changelog}
```
