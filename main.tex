\documentclass[titlepage,a4paper,11pt]{article}

\usepackage{hyperref}
\usepackage{lmodern}
\usepackage{xcolor}
\usepackage{booktabs}
\usepackage{subcaption}
\usepackage{minted}
%%% Change endnotes to enotez
\usepackage{enotez}
\usepackage{fontspec}
\usepackage{booktabs}
\usepackage{tabularx}
\usepackage{subcaption}
\usepackage{titling}
\usepackage[a4paper,margin=3cm]{geometry}
\usepackage{orcidlink}
\usepackage[style=authoryear-comp,backend=biber]{biblatex} %Imports biblatex package

% Fix a todonotes warning
\setlength {\marginparwidth }{2cm} 
\usepackage{placeins}
\usepackage{fancyhdr}
\usepackage{authblk}
\usepackage{siunitx}
%\usepackage{luaquotes}

\usepackage{mdframed}
\usepackage[final]{changes}
\definechangesauthor[name=Jonathan, color=blue]{js}
\definechangesauthor[name=Helle, color=orange]{hp}
\definechangesauthor[name=C, color=black]{c}

% \renewcommand{\chaptermark}[1]{\markboth{\thechapter.\ #1}{}}
\setmainfont{Latin Modern Sans}
\setsansfont{Latin Modern Sans}
\newcolumntype{L}{>{\raggedright\arraybackslash}X}
\addbibresource{main.bib} %Import the bibliography file

%%% Pour relecture papier
% \usepackage{lineno}
% \modulolinenumbers[5]
% \usepackage{setspace}
% \onehalfspacing{}
%%%
\usepackage{subfiles}
\begin{document}
\title{Data Delivery Indicators in EIDA:~Designing a Consistent Metrics System in a Distributed Services Environment}
%% The [] brackets identify the author with the corresponding affiliation. 1, 2, 3, etc. should be inserted.
%% If an author is deceased, please mark the respective author name(s) with a dagger, e.g. "\Author[2,$\dag$]{Anton}{Smith}", and add a further "\affil[$\dag$]{deceased, 1 July 2019}".
%% If authors contributed equally, please mark the respective author names with an asterisk, e.g. "\Author[2,*]{Anton}{Smith}" and "\Author[3,*]{Bradley}{Miller}" and add a further affiliation: "\affil[*]{These authors contributed equally to this work.}".
\author[1]{Jonathan Schaeffer\,\orcidlink{0000-0002-2447-7394}}
\author[1,2]{Helle Pedersen\,\orcidlink{0000-0003-2936-8047}}
\author[3]{Jarek Bienkowski\,\orcidlink{0000-0002-9455-1318}}
\author[4]{Christos Evangelidis\,\orcidlink{0000-0001-8733-8984}}
\author[4]{Vasilis Petrakopoulos\,\orcidlink{0009-0002-9546-1098}}
\author[5]{Javier Quinteros\,\orcidlink{0000-0001-9993-4003}}
\author[5]{Angelo Strollo\,\orcidlink{0000-0001-9602-6077}}
\author{ORFEUS-EIDA Technical Committee and Management Board}

\affil[1]{Univ. Grenoble Alpes, CNRS, INRAE, IRD, METEO-FRANCE, OSUG, 38000 Grenoble, France}
\affil[2]{Univ. Grenoble Alpes, Univ. Savoie Mont Blanc, CNRS, IRD, Univ. Gustave Eiffel, ISTerre, 38000 Grenoble, France}
\affil[3]{Koninklijk Nederlands Meteorologisch Instituut, De Bilt, Netherlands}
\affil[4]{National Observatory of Athens, Greece}
\affil[5]{GFZ German Research Center for Geosciences, Section 2.6, Potsdam, Germany}
%\affil[*]{These authors contributed equally to this work.}
%\affil[t]{Team members listed in Section~\ref{sec:team}}

\date{}
\maketitle

%Gestion des changements: on utiliser le package changes %\url{https://texlive.mycozy.space/macros/latex/contrib/changes/changes.english.pdf}.
%On a défini 2 auteurs hp et js. Pour faire un changement on peut faire:

% \begin{minted}{latex}
% \added[id=hp, comment=Il faut ajouter tout ça]{Ajout de tout un paragraphe}
% \deleted[id=hp]{Texte supprimé}
% \replaced[id=hp]{Nouveau texte}{Ancien texte}
% \highlight[id=hp]{Juste surligner cette partie}
% \comment[id=hp]{Voici un commentaire placé juste là où on veut.}
% \end{minted}                    %

%\listofchanges[style=list, title={List of changes}, show=added|deleted|replaced]

% \listofchanges[style=list, title=List of comments, show=comment]
%\linenumbers{}
\begin{abstract}
The European Integrated Data Archive (EIDA) is a data service within Observatories and Research Facilities for European Seismology (ORFEUS) which distributes seismological waveform data across presently 12 federated data centres, so-called EIDA nodes.
EIDA is also integrated into the European Research Infrastructure EPOS (European Plate Observing System).
The increasing need of quantifying scientific impact of such Research Infrastructures at all levels of operations has led EIDA to develop a unified system able to provide global indicators for data delivery across all EIDA nodes.
To achieve this goal, EIDA implemented a system where the numbers are collected and aggregated by each EIDA node and ingested in a centralised database, accessible through an online API.
The successful implementation of the system stems from carefully identifying the relevant indicators for well defined groups of users.
The implementation combines simplicity and robustness whilst respecting the distributed design of EIDA.
The architecture and implementation choices may be of interest for other multi-site data distribution research infrastructures that face the challenge of providing meaningful Key Performance Indicators.

Keywords: Research data management, Key performance indicators, Research infrastructure, e-infrastructure
\end{abstract}


\subfile{sections/1-introduction}
\subfile{sections/2-tech-framework}
\subfile{sections/3-management-process}
\subfile{sections/4-tech-choices}
\subfile{sections/5-implementation}
\subfile{sections/6-resources-estimation}
\subfile{sections/7-lessons-learnt}
\subfile{sections/8-conclusion}

\appendix
\renewcommand{\thesection}{Appendix \Alph{section}:}
\section*{Additional Files}
The additional files for this article can be found as follows:

\begin{itemize}
  \item Supplementary material 1: Feature matrix of EIDA's KPI system
  \item Supplementary material 2: Guidance to build a KPI system suitable for distributed research infrastructures
\end{itemize}
\subfile{sections/9-code}

% \authorcontribution{Helle Pedersen contributed in designing the KPIs and writing the management part of the article.
%   Jonathan Schaeffer realized the technical design and implementation of the statistics database and supervized the implementation of the web service and the dashboard.
%   Javier Quinteros, Angelo Strollo, Christos Evangelidis and Jarek Bienkowski supervized the implementation of the web service and the dashboard.
%   Vasilis Petrakopoulos designed and implemented the web service and the dashboard.
% } %% this section is mandatory

% \competinginterests{The authors declare that they have no conflict of interest}
\section*{Team List}\label{appendix:team}

This work has been possible with the contribution of EIDA Technical Committee and EIDA Management Board. The list of members is published online (\url{https://www.orfeus-eu.org/data/eida/structure/}).

\section*{Authors contribution}\label{appendix:authorcontribution}
\begin{itemize}
\item Christos Evangelidis, Angelo Strollo, Jarek Bienkowski and Javier Quinteros contributed by closely following the implementation phase giving valuable inputs for the final tool to comply to the end users needs.
\item The EIDA Management Board contributed by submitting all the requirements of the initial project, provide funding sources, and follow up on progresses.
\item The EIDA Technical Committee contributed by deploying and operating the necessary tools on each EIDA node.
\item Jonathan Schaeffer was the technical manager of the project.
\item Vasilis Petrakopoulos wrote and tested the software.
\item Jonathan Schaeffer and Helle Pedersen wrote the article.
\end{itemize}


\section*{Acknowledgements}\label{appendix:ack}

The authors are thankful to the organisations and personnel that operate seismic networks in Europe and across the world and who share their data openly for science.
EIDA also thanks the community of scientific users who use the data from EIDA, and who provide feedback to the EIDA nodes and to the EIDA system as a whole.
EIDA services are integrated in and supported by the European Plate Observing System (EPOS).
Jonathan Schaeffer and Helle Pedersen are personnel of the Epos-France EIDA node which is part of the national research infrastructure Epos-France.
This work has been supported by a grant from Labex OSUG (Investissements d’avenir – ANR10 LABX56). 
We thank two anonymous reviewers for constructive comments.

\section*{Funding Information}\label{sec:fundings}
EIDA development and consolidation have been supported through various EC-funded projects, namely: NERIES, NERA, ENVRI, VERCE, EPOS-PP/IP/SP, EUDAT, SERA, RISE.

The present work was partly supported by Geo-INQUIRE, funded by the European Commission under project number 101058518 within the HORIZON-INFRA-2021-SERV-01 call.

\section*{Competing interests}

The authors have no competing interests to declare.

\newpage
%%% All endnoted end here
\printendnotes*{}

\newpage

\printbibliography{}
\end{document}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% TeX-command-extra-options: "-shell-escape"
%%% ispell-local-dictionary: en_GB
%%% End:
