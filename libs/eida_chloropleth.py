#!/usr/bin/env python3
import sys
import json
import plotly.express as px
import pandas as pd
from pycountry import countries
import requests

worldmap = requests.get('https://www.datahub.io/core/geo-countries/r/countries.geojson').json()

data = requests.get("https://ws.resif.fr/eidaws/statistics/1/dataselect/public?start=2022-01&end=2022-12&details=country&format=json").json()

bytes_data = []
reqs_data = []
clients_data = []
filtered_data = []
for r in data['results']:
    try:
        country = countries.get(alpha_2=r['country']).alpha_3
    except Exception as e:
        print(e)
        continue
    filtered_data.append((country, int(r['bytes']), int(r['nb_reqs']), int(r['clients'])))

df = pd.DataFrame.from_records(filtered_data, columns=['iso_a3', 'bytes', 'nb_reqs', 'clients'])
fig = px.choropleth(df, geojson=worldmap,
                    locations='iso_a3',
                    featureidkey='properties.iso_a3',
                    locationmode='ISO-3',
                    color='bytes',
                    labels={'bytes': 'Bytes'},
                    color_continuous_scale="Viridis_r",
                    scope="world",
                    title="Amount of data delivered to countries",
                    width=1200, height=500
                    )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
fig.write_image('eida_chloropleth_data.png')

fig = px.choropleth(df, geojson=worldmap,
                    locations='iso_a3',
                    featureidkey='properties.iso_a3',
                    locationmode='ISO-3',
                    color='nb_reqs',
                    labels={'nb_reqs': 'Requests'},
                    color_continuous_scale="Viridis_r",
                    scope="world",
                    title="Number of requests issued by countries",
                    width=1200, height=500
                    )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
fig.write_image('eida_chloropleth_reqs.png')

fig = px.choropleth(df, geojson=worldmap,
                    locations='iso_a3',
                    featureidkey='properties.iso_a3',
                    locationmode='ISO-3',
                    color='clients',
                    labels={'clients': 'Clients'},
                    color_continuous_scale="Viridis_r",
                    scope="world",
                    title="Number of clients by countries",
                    width=1200, height=500
                    )
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
fig.write_image('eida_chloropleth_clients.png')
