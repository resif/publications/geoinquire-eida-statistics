#+title: Datamodel

#+begin_src mermaid :file datamodel3.png
classDiagram
   Network --> Node
   Payload --> Node
   Token --> Node
   DataselectStat --> Node
   DataselectStat --> Network
   class Network {
        Integer node_id
        String name
        Boolean inverted_policy
        String eas_group
        }
   class Payload {
        Integer node_id
        BigInteger hash
        String  version
        Timestamp first_stat_at
        Timestamp last_stat_at
        Timestamp generated_at
        Timestamp created_at
        }
   class DataselectStat {
        Integer node_id
        Date date
        Integer network_id
        String  station
        String  location
        String  channel
        String  country
        BigInteger bytes
        Integer nb_requests
        Integer nb_successful_requests
        Integer nb_failed_requests
        HyperLogLog clients
        Timestamp created_at
        Timestamp updated_at
        }
   class Token {
        Integer id
        Integer node_id
        String  value
        Timestamp valid_from
        Timestamp valid_until
        Timestam created_at
        }
   class Node {
        Integer id
        String  name
        String  contact
        Boolean restriction_policy
        String  eas_group
        }

#+end_src

#+RESULTS:
[[file:datamodel3.png]]
