\documentclass[../main.tex]{subfiles}
\graphicspath{{\subfix{../images/}}}

\begin{document}

\section{Technical framework of KPIs collection}\label{sec:tech-framework}

The section provides the basic information needed to understand the technical context and constraints that had to be considered when designing the KPIs system.

Seismological waveform data are identified through a hierarchical naming schema.
Seismic stations are part of a \textit{network}, for which a \textit{network} code is provided by the FDSN.
Within a \textit{network}, the data is then hierarchically organised by \textit{station} (with a \textit{station} code), \textit{location} (with a \textit{location} code) and high-level instrument and time sampling information (with a \textit{channel} code).
As a side note, the need for a \textit{location} code origins in situations where a single station may have multiple instruments, not precisely collocated.
The quadruple \textit{network}|\textit{station}|\textit{location}|\textit{channel} uniquely identifies the data stream. This naming schema is part of the FDSN standards~\parencite{fdsn-source-ids}.

The end users access metadata and data based on FDSN standard web services (respectively \textit{fdsnws-station} and \textit{fdsnws-dataselect})~\parencite{fdsnws-spec} served at all EIDA nodes.
These requests may be sent directly by the end user, programmatically (for instance with the Obspy Python library~\parencite{obspy}) or from other tools such as interactive web interfaces.
Ultimately, all metadata and data requests result in requests to a specific EIDA node over the HTTP protocol.
Almost all data requests are made by programs; interactive access is marginal.
For instance, the Epos-France national seismological data centre registers less than 2\% of requests issued through web browsers.
Web crawlers which scrape web sites by following each possible link cannot issue any valid data request.
Therefore, there is no significant added value of keeping, in the KPI system, a trace of the tools used to access the data (known as the \textit{user agent}).

As a first step, the KPI system focuses on the data delivery service, \textit{fdsn-dataselect}.
This service has a very rich set of options and various selection parameters that can be expressed either in the URL or in the body of an HTTP POST requests.
In this way, the users can cut and slice in the data holdings over multiple time windows and source identifiers.
This makes it impossible to deduce the amount of data distributed grouped by identifiers by observing only web server logs: the shipped data volume needs to be provided by the backend.

In EIDA, data is hosted and distributed through (presently) 12 data nodes.
Discoverability, metadata access and data download is possible through a central routing system.
While most network data are hosted at a single node, some are distributed over several nodes, for example large international seismological experiments operating under a single network code, and the splitting of the data streams can go down to channel level.
In this distributed context, the high level KPIs for data usage of a seismological network has to be aggregated over several nodes.

To be part of the EIDA federation, a node has to provide all the waveform related services described by the FDSN web services specification~\parencite{fdsnws-spec}.
Although a majority of nodes deploy the same software stack~\parencite{seiscomp}, other nodes have their own implementation.
For the KPI system, the requirement for all EIDA nodes is to produce a detailed log of the requests in a specified text format.
As a side note, each node has operational monitoring systems adapted to their local context, so the KPI system is not aimed at operational use cases.

Only a few of the data sets distributed in EIDA are not available in open access.
Therefore, almost all data requests are issued anonymously and the only information that can account for the number of users is the origin’s IP address.
We discuss the limitation of this information in Section~\ref{sec:user-proxy}.

The first generation of data distribution system for EIDA (now retired Arclink\endnote{Arclink: \url{https://www.seiscomp.de/seiscomp3/doc/applications/arclink.html}}) had the capacity of providing basic KPIs values based on daily emails from each EIDA node and aggregated by the EIDA node hosted at the German Research Centre for Geosciences in Potsdam~\parencite{GFZ}.
When EIDA migrated to the standardised FDSN web services, this capacity was not immediately implemented.
Designing a new system for key indicators across EIDA, rather than reproducing the functionalities of the old one, was therefore a priority for EIDA.

\end{document}


%%% Local Variables:
%%% mode: latex
%%% ispell-local-dictionary: en_GB
%%% End:
