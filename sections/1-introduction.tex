\documentclass[../main.tex]{subfiles}
\graphicspath{{\subfix{../images/}}}

\begin{document}

\section{Introduction}


ORFEUS (Observatories and Research Facilities for European Seismology\endnote{ORFEUS:~\url{http://orfeus-eu.org}}) is a non-profit foundation that promotes seismology in the Euro-Mediterranean area through the collection, storage and distribution of seismic waveform data, metadata, and closely related services and products.
It is also the European organisation to distribute seismological waveform data within the European Plate Observing System (EPOS\endnote{EPOS:~\url{https://www.epos-eu.org}}).
The European Integrated Data Archive~\parencite{EIDA} within ORFEUS is a distributed system which provides access to seismic waveform data from European archives.
EIDA's role is to ensure federated data and metadata delivery services, organise EPOS integration and manage interactions with the FDSN.
EIDA has been described by~\textcite{10.1785/0220200413}.

Data is hosted across 12 European seismological data centres, so-called EIDA nodes\endnote{List of EIDA nodes \url{http://www.orfeus-eu.org/data/eida/nodes/}} and made available through the same inter-operable methods.
In the following, we use the term data centre and EIDA node interchangeably.
EIDA as a whole fully complies to domain specific international standards as defined by the International Federation of Digital Seismograph Networks (FDSN\endnote{FDSN \url{https://www.fdsn.org}}). 
The requirements for a node to join EIDA is to respect FDSN data and metadata standards and operate a defined set of protocols and  services.
Each EIDA node has different data management and data distribution infrastructure, software, data volumes, staffing level, institutional and national constraints.

It is becoming increasingly important to be able to demonstrate the scientific impact of the EIDA services at regional, national and European levels, both for institutional support and for funding purposes.
Therefore, the EIDA Management Board (EMB) continuously dedicates effort to improve knowledge on impact.
In the context of distributed research infrastructures, such as many European Infrastructures on the European Strategy Forum on Research Infrastructures (ESFRI) road map\endnote{ESFRI road map 2021: \url{https://roadmap2021.esfri.eu/}}, the indicators need to be homogenised across many organisations to build consistent Key Performance Indicators (KPIs).

EIDA is an ideal test case for developing such systems, as it has a very strong and mature federative culture and a moderate number of federated data centres (presently 12, and still growing).

EIDA presently has two complementary strategies to provide insight on impact and usage.
The first one is to strongly support the attachment of persistent identifiers (DOI) to each dataset and to link them to citation metadata (authors, funders, stakeholders,~\ldots).
For waveform data, FDSN has published a set of recommendations~\parencite{why-doi} updated in 2023~\parencite{fdsn-doi-2023} and provides a DOI minting service (operated by EarthScope~\parencite{earthscope}) and citation tool.
ORFEUS supports the implementation of DOIs for seismic networks, both by helping networks who need identifiers and by undertaking actions towards publishers, through collaboration with partner organisations, and through interaction with scientific users of the network data.
Usage information from that channel is increasing but it is still sparse because data citations through identifiers are still gaining traction in the community.
Once the networks are systematically cited through these identifiers it will be possible to understand better the \textit{use of data}: scientific publications versus private sector reports, areas of research, connection to publication keywords, etc.

A second strategy, which is the subject of this article, is to quantify service usage through logging of metadata and data requests in order to build a set of key indicators.
Carefully chosen key indicators can give a quantified view (albeit approximate) of the number of users and their geographical distribution, shipped data volumes and number of requests. 

In parallel, the KPI needs for research infrastructures is progressively being harmonized, in particular after the publication of the ESFRI Report `Monitoring of Research Infrastructures Performance'~\parencite{esfri-kpi-report}.
It highlights that the KPIs applied to a Research Infrastructure should fulfil the `RACER' criteria (Relevant, Accepted, Credible, Easy to Monitor), and promotes 21 indicators.
Most of these are mainly adapted to single site physical Research Infrastructure where the applicants, users, user profile, and ensuing results (scientific articles for example) are well known.
Some of them however are relevant for data services and distributed research infrastructures where services are accessed anonymously by a large number of users worldwide.
In particular, the KPI 1 (Number of user requests for access) and 2 (Number of users served) can be evaluated through the monitoring of service usage.
Additional KPIs depend on knowing the geographical location of users (KPI 8, `Share of users and publications per ESFRI member country', and KPI 17, `Share of users and publications per non-ESFRI member country') can at least be partly estimated through service usage monitoring.
These and additional KPIs may be relevant across different institutions and countries.

The KPI collection and aggregation requires that all of the EIDA nodes agree on a set of prioritised service usage KPIs that are useful to EIDA, and to all or a subset of nodes and other potential users of the KPIs.
Additionally, a strong constraint is that each node can provide the logs used for the KPIs calculation.
This is a common challenge across most EPOS Thematic Core Services and other distributed infrastructures, which is why it is subject of dedicated harmonisation efforts within the European project Geo-INQUIRE\endnote{Geo-INQUIRE:~\url{https://www.geo-inquire.eu}, WP7}.
Discussions within this context, across almost all EPOS Thematic Services and several other distributed Research Infrastructures, demonstrated the necessity and the challenge of adopting and operating a KPI system aggregated across several data centres, and of being able to serve KPIs at different granularity to different users.


This article addresses how EIDA designed a federated KPI system where metrics are collected locally at each node, and consolidated in a central database. The federated KPI system has been operational since April 2021.
Today, EIDA is in a good position to evaluate the process, strengths and weaknesses of the system, and to share the lessons learnt with a larger community; this sharing of practical experience is the main objective of the article.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% ispell-local-dictionary: en_GB
%%% End:
