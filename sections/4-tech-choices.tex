\documentclass[../main.tex]{subfiles}
\graphicspath{{\subfix{../}}}

\begin{document}

\section{Technical Analysis of the Requirements and Strategic Technical Choices}\label{sec:tech_analysis}

With the requirement matrix finalised by the EMB, the ETC was able to consider implementation choices and identify possible technical problems.
Also, the ETC could evaluate the resources necessary for the software development.
Besides, acknowledging the important share of Information and Communication Technology in the global environmental crisis~\parencite{Istrate_2024}, the technical design aims at minimizing the footprint of the implementation.
In this section we present the main challenges and the strategic choices made before the implementation.

\subsection{Data delivery estimation}\label{sec:data-estmimate}

Many open source solutions provide tools for HTTP traffic analysis.
Usually, inside an HTTP web server, it is easy to track the request parameters appearing as options in the URL, the status of the request (successful or in error), and the amount of data delivered to the client.
But, due to our technical context, detailed in Section~\ref{sec:tech-framework}, those tools are not fit for the EIDA KPI system.

Figure~\ref{fig:logging1} illustrates the logging methods on the web server and on the dataselect engine and showcase the information that exists at the two levels.
In the webserver logs, the response size is a global value that cannot be attributed to source identifiers.
On the contrary, the data delivery logs provide the number of bytes delivered for each source identifier.

\begin{figure}[ht]
  \centering
  \includegraphics[width=12cm]{images/fig01.pdf}
  \caption{\label{fig:logging1}Illustration of how a request is logged in an EIDA node: (1) a request with a set of constraints arrives to the web server; (2) the request is transmitted to the fdsnws-dataselect engine that analyses the constraints, finds the matching data; (3) the data are delivered; (4) the data shipped are analysed in order to write a data shipment report; (5) the web server logs the request. Note that in the webserver log, the response size is a global value that cannot be attributed to source identifiers, while the data delivery log provides the number of bytes delivered for each source identifier.}
\end{figure}

\subsection{Number of different users}\label{sec:user-proxy}

Another request from the EMB is to know how many different users are using data from EIDA.
There are several challenging issues with this request.

In order to evaluate the number of distinct clients to the service, the EIDA nodes need to identify the user of each request. This identification can be made in two ways:

\begin{itemize}
  \item when the data request is authenticated, the EIDA node retrieves the email address of the user
  \item when the data request is anonymous, the EIDA node retrieves the IP address.
\end{itemize}

\subsubsection{Approximating the number of users by the number of distinct IP addresses}

In the context of generally anonymous data access, there is no way to identify an individual. As a user can be either a person or a computer program, we agreed to map the concept of a user to the IP address. Therefore, each unique IP address is considered as a user.
Sometimes we refer to users as clients, both words designating this proxy.
This information can, at best, be a proxy for the unique users evaluation due to two limitations:
\begin{itemize}
  \item The same user can use several IP addresses to request data (for instance from a computing cluster, or Dynamic Host Configuration Protocol (DHCP));
  \item The same IP address is used by several people, in the case of a centralised server with many users like HPC platforms or Network Address Translation (NAT);
\end{itemize}
Over time, the data access methods may evolve with more HPC usage for instance, changing the proportion of real user behind one IP address.

In spite of these shortcomings, the number of different IP addresses gives a meaningful approximation of the number of users, by giving a reasonable order of magnitude.

\subsubsection{Anonymising the logs}\label{sec:anonymise}

Both email and IP addresses are considered as personal information and thus, the system should take care of privacy considerations, and comply to the General Data Protection Regulation (GDPR\endnote{GDPR details: \url{https://eur-lex.europa.eu/legal-content/EN/TXT/PDF/?uri=CELEX:32016R0679}}).
We manage this by anonymising the IP addresses in each node, followed by an additional layer (hyperLogLog, see below) before shipping any information to the central node.
In this way, each node is fully responsible for respecting GDPR locally, while the central node does not handle data that falls under GDPR legislation.

Therefore, during the data logging process at the fdsnws-dataselect engine level, the user's email or the IP address are digested to compute a user identifier.
This process is done using a hash function\endnote{Hash function definition: \url{https://en.wikipedia.org/wiki/Hash_function}}, and guarantees that:
\begin{itemize}
  \item the same input always results in the same user ID (it is deterministic),
  \item \replaced{it is difficult to retrieve the initial information by making a reverse computation}{it is not possible to deduce the initial information} (non reversible one-way function\endnote{Definition of a one way function: \url{https://en.wikipedia.org/wiki/One-way_function}}).
\end{itemize}

Note that this hashing algorithm is common to all the implementation across all EIDA nodes\endnote{Example of the implementation of user ID hash in seiscomp's source code \url{https://github.com/seiscomp/main/blob/3c11014d49dbefd29ccaa436647cb1aa0727482f/apps/fdsnws/fdsnws/reqlog.py\#l36} }, otherwise the number of users across EIDA becomes meaningless.

\subsubsection{Computing the cardinality of unique users on target time windows}\label{sec:cardinality}

The unique users indicator is specific because the sum of unique users across all the aggregates or for large time windows would count the same users several times.
It is a very common use case to retrieve seismological data from several EIDA nodes simultaneously.
This is usually known as the \textit{count-distinct problem}\endnote{Definition of the count-distinct problem \url{https://en.wikipedia.org/wiki/Count-distinct_problem}}.

To take into account multiple occurrences of the same IP address when aggregating time windows, we used the HyperLogLog~\parencite{flajolet:hal-00406166} algorithm which can aggregate together sets of information and evaluate very efficiently the cardinality of elements in them, with an acceptable accuracy for the project (2\% standard error).

The resulting object does not carry the original information (in our case a hashed IP address or user email) but only sets of numbers that allow to estimate a cardinality.\deleted{ It is therefore not possible from this information to retrieve the original data.}
This property is particularly interesting because the values manipulated by the central system does not contain any personal data, not even the hashed IP address.

The requirements issued by the management process states that time granularity of the indicators should be by month and by year.
The aggregative property of HyperLogLog sets enables the possibility to minimise the size of the database according to the initial intention of the project to minimise its ecological footprint.
Therefore, the indicators in the central database are aggregated by month (see Section~\ref{sec:tech-step3} for implementation details)

\subsection{Aggregation over EIDA nodes}

Three strategies have been discussed when designing the federated indicators system:
\begin{itemize}
    \item fully centralised: all logging information is shipped and centralised by a single node;
    \item fully distributed (distribution of logging information from each EIDA node, with clients built on top for the aggregate, build indicator values and make them accessible);
    \item hybrid approach: logging information is aggregated locally on each EIDA node, aggregated values are shipped and centralised by a single node.
\end{itemize}
Finally, the latter model was chosen, based on the advantages and disadvantage set out below.

\subsubsection{Centralised storage and exploitation of logging information}

\begin{figure}
    \centering
    \includegraphics[width=12cm]{images/fig02.pdf}
    \caption{In a central logging architecture, each EIDA node, for each request, sends the information to the central system.}\label{fig:centralized_log}
\end{figure}

The centralisation of storage and exploitation implies that each data centre sends all collected data request information to a single EIDA node which takes the responsibility of storing and computing indicator values based on all the logs.
The advantage of centralising each data query event through a centralised database is that it enables new possibilities like carrying out knowledge discovery on the data requests patterns, or even alerting in real time.
This architecture is shown in Figure~\ref{fig:centralized_log}.

Today, major open source solutions such as OpenSearch\endnote{OpenSearch: \url{https://opensearch.org}} bring interesting features and capabilities for managing this amount of information, but it was not the case at the time of the conception of our architecture.

\subsubsection{Distributed storage and exploitation of logging information}

A fully distributed KPIs system would mean that each data centre would maintain an access endpoint for the the end user (for example EIDA management, EIDA nodes managers and network managers) to fetch values, in a similar strategy to those of the metadata or data distribution web services.
The main advantage is that each EIDA node can manage its logging system independently.

The main drawback is that it becomes costly to develop and maintain the tools that allows a user to obtain the indicator values interactively across all nodes, and consolidate high-level information across all or a subset of nodes.
A fully distributed system also requires continuous uptime across all EIDA nodes for the service to be effectively available to the user at all times.
Such efforts can be made for priority services, such as metadata and data access for scientific users, but is unrealistic for a service which is not top priority for each EIDA node.
The risk of failure of continuous service at any one of the EIDA nodes is therefore too high, considering the low priority that this service has in daily operations.

\subsubsection{Distributed aggregation of logging information, centralised storage of aggregates}\label{sec:strategy3}
An intermediate approach is to make each data centre responsible for computing periodically some logs statistics and ship the result to a central service, which can then publish aggregated values to the end users. The main advantages are:
\begin{itemize}
%\item Anonymisation is carried out at each node before shipment (GDPR compliance);
\item Computing and storage are managed at each node (local hardware constraints respected);
\item Volume of shipped logging information remains small;
\item Periodic rather than continuous ingestion of EIDA node logging information (no need for continuous uptime of the service at all nodes and relaxed uptime constraints on the central node).
\end{itemize}

It remains possible in this semi-centralised system to clearly identify the responsibility of the individual EIDA nodes as well as of the central node that operates the EIDA-wide central system:
Once the high level logging aggregates are successfully shipped to the central system, the EIDA node can discard it, leaving the hosting responsibility to the central system.
The semi-centralised system still leads to a straightforward software architecture across the whole federation, from logging collection to the final usage of the system.
The workflow is illustrated in figure~\ref{fig:flowchart}, the aggregation process is described in more details in Section~\ref{sec:to_stat}.

\begin{figure}
  \centering
  \includegraphics[width=12cm]{images/fig03.pdf}
  \caption{\label{fig:flowchart} Workflow of building data usage indicators from initial logs. The Aggregator program transforms a set of data delivery events into aggregated values where a subset of individual values are grouped by month, source identifiers and country of origin of the requests. The Aggregator program adds the EIDA node as additional metadata. The aggregated logs are sent to the central service as a payload. The central KPI system ingests the payload by aggregating it to the existing values.}
\end{figure}

\subsubsection{Final aggregation strategy choice}
Given the previous analysis of the various options at hand, ETC implemented a semi-centralised system, i.e.~an architecture of local aggregation, with central storage for aggregates, as described in Section~\ref{sec:strategy3}.

\end{document}

%%% Local Variables:
%%% mode: latex
%%% ispell-local-dictionary: en_GB
%%% End:
